local Discordia = require('Discordia')
local Configuration = require('BotConfig/Config')
local BotClient = Discordia.Client()

BotClient:on('ready', function()
    print('The ready event was fired.')
end)

BotClient:run('Bot ' .. Configuration.BotToken)